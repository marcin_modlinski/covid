const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const combineDuplicatedCssSelectors = require('postcss-combine-duplicated-selectors');
const discardCssComment = require('postcss-discard-comments');
const BundleTracker = require('webpack-bundle-tracker');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  context: __dirname,
  entry: [
    './assets/js/app.js',
    './assets/scss/app.scss'
],
  output: {
      path: path.resolve('./assets/bundles/'),
      filename: "[name]-[hash].js"
  },
  
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
              presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.s[ac]ss$/i,
        use: [ 
          'style-loader', 
          MiniCssExtractPlugin.loader, 
          {
            loader: 'css-loader',
            options: {
              url: false,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [autoprefixer(), discardCssComment(), combineDuplicatedCssSelectors()]
            }
          },
          'sass-loader'
        ]
      }
    ]
  },  

  plugins: [
    new CleanWebpackPlugin(),
    new BundleTracker({filename: './assets/bundles/webpack-stats.json'}),
    new MiniCssExtractPlugin({
      filename: '[name]-[hash].css',
      chunkFilename: '[id].css',
    }),
    new CopyPlugin({
      patterns: [
        { from: './assets/fonts', to: 'fonts' },
        { from: './assets/images', to: 'images' },
      ],
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
     })
  ]
}