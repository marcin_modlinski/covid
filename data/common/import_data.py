from common.apify import Apify
from data.models import Case, Region, CaseByRegion
import json
import datetime


class ImportData(object):
    __apify = Apify()


    def __get_data_from_api(self, number=1):
        data = self.__apify.get_last_items(number)
        data_to_json = json.loads(data)
        # Posortowane dane (od najstarszych)
        return sorted(data_to_json, key = lambda i: i["lastUpdatedAtApify"], reverse=False)


    def __set_region(self, region_name):
        region = None
        try:
            region = Region.objects.get(name=region_name)
        except Region.DoesNotExist:
            if ("" != region_name):
                region = Region(name=region_name)
                region.save()

        return region


    def __set_data_to_regions(self, regions, caseObject):
        regions_to_save = {}
        for item in regions:
            if ("region" in item):
                # Stworzenie słownika postaci: {'opolskie': {'infected': 3, 'deceased': 0}}
                # Z uwagi na możliwe wystąpienie kilka razy regionu w rekordzie - sumujemy wartości
                regionObject = self.__set_region(item["region"])
                if (None != regionObject):
                    regions_to_save[regionObject] = regions_to_save[regionObject] if item["region"] in regions_to_save else {}
                    regions_to_save[regionObject]["infected"] = regions_to_save[regionObject]["infected"] + item["infectedCount"] if item["region"] in regions_to_save and "infected" in regions_to_save[item["region"]] else item["infectedCount"]
                    regions_to_save[regionObject]["deceased"] = regions_to_save[regionObject]["deceased"] + item["deceasedCount"] if item["region"] in regions_to_save and "deceased" in regions_to_save[item["region"]] else item["deceasedCount"]

        for to_save in regions_to_save:
            region = CaseByRegion(
                case=caseObject,
                region=to_save,
                infected=regions_to_save[to_save]["infected"],
                deceased=regions_to_save[to_save]["deceased"]
            )
            region.save()

        return regions_to_save

    
    def get_api_summary(self):        
        return self.__apify.get_summary()


    def set_data_from_api(self, number=1):
        data = self.__get_data_from_api(number)
        for item in data:
            element = Case(
                infected=item["infected"], 
                deceased=item["deceased"], 
                last_updated=item["lastUpdatedAtApify"]
            )
            element.save()

            if ("infectedByRegion" in item):
                self.__set_data_to_regions(item["infectedByRegion"], element)

        return True
