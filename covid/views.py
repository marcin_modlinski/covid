from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from data.common.import_data import ImportData
import json
from data.models import Case


def index(request):
    import_data = ImportData()
    data = json.loads(import_data.get_api_summary())
    api_item_count = data["data"]["cleanItemCount"]
    db_item_count = Case.objects.count()

    return render(request, 'base.html', {'api_item_count': api_item_count, 'db_item_count': db_item_count})