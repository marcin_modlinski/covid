$(document).ready(function () {
    let saveData = $('.js-get-from-api');
    let deleteData = $('.js-delete-from-db');
    let dbItemCount = $('.js-db-item-count');
    let apiItemCount = $('.js-api-item-count');
    let progressbar = $('.js-db-item-count + .progress');

    function toast(message = '') {
        return M.toast({html: message, classes: 'rounded'});
    }

    function lockNextAction(e) {
        progressbar.toggleClass('hide');
        e.preventDefault();
    }

    function updateCount(data) {
        apiItemCount.text(data.api_item_count);
        dbItemCount.text(data.db_item_count);
        progressbar.toggleClass('hide');
    }

    saveData.on('click', function(e) {
        toast('Rozpoczęto pobieranie danych');
        lockNextAction(e);
        $.ajax({
            url: '/data/download_data',
            success: function (data) {
                updateCount(data);
                toast('Dane zostały pobrane');
            }
        });
    });

    deleteData.on('click', function(e) {
        toast('Rozpoczęto usuwanie danych');
        lockNextAction(e);
        $.ajax({
            url: '/data/delete_data',
            success: function (data) {
                updateCount(data);
                toast('Dane zostały usunięte');
            }
        });
    });
});