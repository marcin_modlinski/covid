from django.contrib import admin
from django.urls import path
from . import views

app_name = 'data'

urlpatterns = [
    path('download_data', views.download_data, name='data-download_data'),
    path('delete_data', views.delete_data, name='data-delete_data')
]