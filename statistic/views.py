from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from data.common.data_repository import DataRepository
from svg.charts.plot import Plot
import json
from datetime import datetime

def simply_grow(request):
    repository = DataRepository()
    dataRepo = repository.get_cases()
    data = []
    for item in dataRepo:
        data.append(datetime.timestamp(item['last_updated']))
        data.append(item['infected'])

    plot = Plot({
        'height': 500,
        'width': 800,
        'key': True,
        'scale_x_integers': True,
        'scale_y_integerrs': True,
        'graph_title': "TS Title",
        'show_graph_title': True,
        'scale_x_integers': True,
        'scale_y_integers': True,
        'show_x_guidelines': False,
        'show_data_values': False,
        'graph_background': 'white'
    })

    plot.add_data({
        'data': data,
        'title': 'Liczba zakażonych',
    })

    return render(request, 'simply_grow.html', {'status':data, 'plot':plot.burn()})
