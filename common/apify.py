import requests


class Apify(object):
    __address = 'https://api.apify.com/v2/datasets'
    __key = '/L3VCmhMeX0KUQeJto'
    __collection = '/items'
    
    
    def get_summary(self):
        address = self.__address + self.__key
        response = requests.get(address)
        if (True != response.ok):
            # raise Exception('Błąd połączenia z API. Kod odpowiedzi: ' . response.status_code)
            return False

        return response.text

    
    def get_all_items(self):
        address = self.__address + self.__key + self.__collection
        response = requests.get(address)
        if (True != response.ok):
            return False

        return response.text

    
    def get_last_items(self, number=1):
        address = self.__address + self.__key + self.__collection
        response = requests.get(address, params={'format': 'json', 'limit': number, 'desc': '0'})
        if (True != response.ok):
            return False

        return response.text