from django.urls import path
from . import views

app_name = 'statistic'

urlpatterns = [
    path('simply_grow', views.simply_grow, name='statistic.simply_grow'),
]