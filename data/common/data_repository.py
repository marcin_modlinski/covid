from data.models import Case


class DataRepository(object):


    def get_cases(self):
                return Case.objects.all().values('last_updated', 'infected') 