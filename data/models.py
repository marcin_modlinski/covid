from django.db import models


class Case(models.Model):
    infected = models.IntegerField()
    deceased = models.IntegerField()
    last_updated = models.DateTimeField()
    created = models.DateTimeField(auto_now_add=True)


class Region(models.Model):
    name = models.CharField(max_length=100, unique=True)
    created = models.DateTimeField(auto_now_add=True)


class CaseByRegion(models.Model):
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    infected = models.IntegerField()
    deceased = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = __package__.rsplit('.', 1)[-1] + "_case_by_region"