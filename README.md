# COVID
Aplikacja oparta o język Python oraz framework Django.

- [COVID](#covid)
  - [Opis](#opis)
  - [Technologie](#technologie)
  - [Zależności](#zależności)
  - [Instalacja](#instalacja)
    - [Podstawowa](#podstawowa)
    - [Rozszerzona (tylko dla orłów)](#rozszerzona-tylko-dla-orłów)
  - [Develop](#develop)

## Opis
Statystyka dotycząca zakażeń

## Technologie
* Python >= 3.6
* Python PIP
* yarnpkg

## Zależności
* Django >= 3.0
* psycopg2 >= 2.7, <3.0
* requests
* svg.charts
* django-webpack-loader

## Instalacja
### Podstawowa
1. Instalacja środowiska.  
Zainstalować biblioteki systemowe:  
`apt update`  
`apt install -y python3 python3-pip git`  
Sprawdzić, czy zostały zainstalowane (polecenia powinny zwrócić wersje bibliotek):  
`python3 --version; pip3 --version; git --version`  
Zainstalować pakiety Pythona:  
`pip3 install Django psycopg2-binary requests django-webpack-loader`
2. Utworzenie/skopiowanie projektu.  
a. Przechodzimy do katalogu, w którym chcemy utworzyć projekt (gdziekolwiek: w Dokumentach, na Pulpicie). Nie musimy tworzyć katalogu projektu, zostanie on stworzony w następnym kroku. W ktalogu wydajemy polecenie (nastąpi skopiowanie projektu z repozytorium):  
`git clone https://marcin_modlinski@bitbucket.org/marcin_modlinski/covid.git`  
b. Wchodzimy do katalogu skopiowanego projektu i wydajemy w terminalu polecenie:  
`python3 manage.py migrate`  
Następnie uruchamiamy serwer www poleceniem:  
`python3 manage.py runserver`  
Terminal poda nam adres, pod którym w przeglądarce będzie działał projekt. Dodatkowo w terminalu logowane są żądania kierowane do serwera. Jak widać, aby zatrzymać serwer należy w terminalu wydać polecenie `CTRL+C`.
### Rozszerzona (tylko dla orłów)
1. Modyfikacja wyglądu projektu.  
Należy zainstalować menadżer pakietów yarn:  
`sudo apt install -y curl gnupg gnupg2 gnupg1`  
`curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -`  
`curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -`  
`echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`  
`sudo apt update`  
`sudo apt install -y nodejs yarn`

## Develop
1. Aplikacje.
Trzon projektu znajduje się w katalogu `covid` i to w nim zawarta jest konfiguracja i pliki rozruchowe całego projektu. Główny katalog (w znaczeniu Django) jest projektem.  
Na projekt składają się aplikacje, jest nim też `covid`, które znajdują się w osobnych katalogach. Każda z takich aplikacji powinna robić jedną rzecz. Np. obsługa userów powinna być osobną aplikacją, albo zarządzanie produktem w sklepie internetowym, albo wpis na blogu - najczęściej do każdej aplikacji kieruje inna część URL (projekt/user, projekt/produkt).  
Aby rozpocząć tworzenie nowej funkcjonalności (aplikacji) należy wydać w terminalu polecenie:  
`python3 manage.py startapp nazwa_aplikacji`  
spowoduje to stworzenie struktury katalogu. Jak widać - aplikacje są po prostu pythonowym pakietem.  Więcej: https://docs.djangoproject.com/pl/3.1/intro/tutorial01/  
Poza aplikacjami, wykonującymy określone i odseparowane akcje w projekcie, możemy tworzyć moduły, które są współdzielone (importowane) przez wiele aplikacji.  
Nalepiej tworzyć je jako zwyłe pythonowe moduły w katalogu projekt/common.  
Więcej: https://www.learnpython.org/pl/Moduly_i_pakiety
2. Git.  
Do wersjonowania kodu wykorzystano git.  
Na początku należy skonfigurować git:  
`git config --global user.name "imię nazwisko"`  
`git config --global user.email "email"`  
Wszystkie zmiany w kodzie są rejestrowane. Wystarczy wprowadzić zmianę w dowolnym pliku i wydać polecnie `git status`, aby przekonać się, że dokonano zmian.  
Aby zapisać stan repozytorium, tj. zatwierdzić wprowadzone zmiany należy wydać polecenia:  
`git add .`  
`git commit -m "treść wiadomości opisującej zmiany w kodzie"`  
Następnie tak wprowadzone zmiany można wysłać na serwer repozytorium:  
`git push` lub `git push origin master`  
Aby pobrać z tego serwera zmiany w kodzie do swojego projektu, należy pamietać, że `git status` nie może wykazać zmienionych plików. Wszystkie dotychczasowe zmiany w kodzie muszą zostać _wypchnięte_ na serwer. Pobranie z serwera zmian odbywa się za pomocą polecenia:  
`git pull` lub `git pull origin master`  
3. Wygląd (frontend - dla orłów)  
Wizualna część aplikacji opiera się o plik określający wygląd aplikacji (CSS) oraz jej dynamiczne zachowanie/akcje, plik JS. W celu wygodnego zarządzania stroną wizaulaną wykorzystano nowoczesne narzędzie `WebPack` (pakiet pobierany yarnem). Dokonuje ono transkrypcji, łączenia i minifikacji plików CSS i JS. Podanto moduł Pythona `django-webpack-loader` dokonuje automatycznego wstawiania wygenerowanych przez WebPack plików do stuktury aplikacji.  
WebPack wykorzysuje tworzone podczas pisania aplikacji pliki zebrane w folderach assets/js oraz assets/scss i generuje pliki wynikowe w folderze assets/bundles (zgodnie z wytycznymi zebranymi w pliku `webpack.config.json`).  
WebPack jest biblioteką JS. Drugim narzędziem wykorzystywanym w projekcie jest yarn - instalator bibliotek JS. Wykorzystuje on listę wymaganych w projekcie bibliotek zebranych w pliku `package.json` i instaluje je w folderze `node_modules`. Z bibliotek tych korzysta WebPack.  
Aby rozpocząć korzystanie z WebPack należy zainstalować yarn ([Instalacja](#instalacja)) i zainstalować wymagane pakiety JS:  
`yarn install`  
oraz zbudować pliki wynikowe poleceniem:  
`yarn build` lub `yarn buil-prod`.  
 Polecenie `yarn build` generuje pliki w postaci nieskompresowanej, pomocnej podczas prac programistycznych, natomiast polecnie `yarn buil-prod` generuje pliki zoptymalizowane (pod kątem środowiska produkcyjnego).