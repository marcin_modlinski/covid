from django.http import JsonResponse
from data.common.import_data import ImportData
import json
from data.models import Case, Region, CaseByRegion

def download_data(request):
    import_data = ImportData()
    data = json.loads(import_data.get_api_summary())
    api_item_count = data["data"]["cleanItemCount"]
    db_item_count = Case.objects.count()

    if (api_item_count > db_item_count):
        # Pobranie x brakujących, ostatnich rekordów z API
        to_save = import_data.set_data_from_api(api_item_count - db_item_count)

    return JsonResponse({'api_item_count': api_item_count, 'db_item_count':Case.objects.count()})


def delete_data(request):
    import_data = ImportData()
    data = json.loads(import_data.get_api_summary())
    api_item_count = data["data"]["cleanItemCount"]
    db_item_count = Case.objects.count()
    Case.objects.all().delete()
    Region.objects.all().delete()
    Case.objects.raw('UPDATE sqlite_sequence SET seq = 1  WHERE name="data_case";')
    Region.objects.raw('DELETE FROM SQLite_sequence WHERE name=data_region;')
    CaseByRegion.objects.raw('DELETE FROM SQLite_sequence WHERE name=data_case_by_region;')

    return JsonResponse({'api_item_count': api_item_count, 'db_item_count':Case.objects.count()})